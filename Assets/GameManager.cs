﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int Score
    {
        get { return score; }
        set
        {
            score = value;
            Label.text = "Score " + score.ToString();
        }
    }
    private int score;



    public int DartCount = 10;

    public Text Label;
    public Button RestartButt;
    public Text RestartButtText;
    public GunController Gun;

    public void RestartLevel()
    {
        Gun.RemainingDarts = DartCount;
        this.Score = 0;
        foreach (var item in GameObject.FindGameObjectsWithTag("Dart"))
        {
            item.GetComponent<hObjectPoolItem>().DespawnSafely();
        }
        RestartButt.interactable = false;

        Invoke("ReloadGun", 0.2f);
        //Application.LoadLevel(Application.loadedLevel);
    }

    public void ShowScore()
    {
        RestartButt.interactable = true;
        RestartButtText.text = string.Format("Score\n< {0} >\nAgain!", Score);
        Gun.enabled = false;
    }

    public void ReloadGun()
    {
        Gun.enabled = true;
    }

    public void Start()
    {
        ReloadGun();
    }

    public void Update()
    {
        if (Gun.RemainingDarts == 0)
        {
            ShowScore();
        }
    }
}
