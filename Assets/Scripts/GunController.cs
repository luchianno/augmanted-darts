﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GunController : MonoBehaviour
{
    public GameObject Dart;
    public float Force;

    public int RemainingDarts;

    public Button RestartButt;

    void Start()
    {
    }

    Vector2 posStart;
    void Update()
    {
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    posStart = touch.position;
                    break;
                case TouchPhase.Canceled:
                    break;
                case TouchPhase.Ended:
                    Swipe(touch.position - posStart);
                    break;
                case TouchPhase.Moved:
                    break;
                case TouchPhase.Stationary:
                    break;
                default:
                    break;
            }
        }

        if (Input.GetButtonUp("Jump"))
        {
            Swipe(Vector2.zero);
        }

        if (Input.GetMouseButtonDown(0))
        {
            posStart = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector2 temp = Input.mousePosition;
            Swipe(temp - posStart);
        }
    }

    void Swipe(Vector2 direction)
    {
        var rotY = -Mathf.Sign(direction.y) * Mathf.Lerp(0, 25, (float)Mathf.Abs(direction.y) / 500f);
        var rotX = Mathf.Sign(direction.x) * Mathf.Lerp(0, 25, (float)Mathf.Abs(direction.x) / 500f);
        transform.localRotation = Quaternion.Euler(rotY, rotX, 0);

        Shoot();
        //Debug.Log("dir " + direction + " angle " + rot);
    }

    public void Shoot()
    {
        if (RemainingDarts != 0)
        {
            RemainingDarts--;
            var temp = hObjectPool.Instance.Spawn(Dart, this.transform.position, this.transform.rotation);
            //var temp = (GameObject)Instantiate(Dart, this.transform.position, this.transform.rotation);
            //temp.transform.parent = GameObject.Find("DartsPool").transform;

            temp.rigidbody.velocity = temp.transform.forward * Force;
        }
    }
}
