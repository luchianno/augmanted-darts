﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    public GameObject Gun;

    Vector2 posStart;
    void Update()
    {
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    posStart = touch.position;
                    break;
                case TouchPhase.Canceled:
                    break;
                case TouchPhase.Ended:
                    Swipe(touch.position - posStart);
                    break;
                case TouchPhase.Moved:
                    break;
                case TouchPhase.Stationary:
                    break;
                default:
                    break;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            posStart = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector2 temp = Input.mousePosition;
            Swipe(temp - posStart);
        }
    }

    void Swipe(Vector2 direction)
    {
        var rot = -Mathf.Sign(direction.y) * Mathf.Lerp(0, 45, (float)Mathf.Abs(direction.y) / 500f);
        Gun.transform.localRotation = Quaternion.Euler(rot, 0, 0);

        Gun.GetComponent<GunController>().Shoot();

        Debug.Log("dir " + direction + " angle " + rot);
    }
}
