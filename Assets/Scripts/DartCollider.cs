﻿using UnityEngine;
using System.Collections;

public class DartCollider : MonoBehaviour
{
    public Transform Tip;

    void Update()
    {
        this.transform.forward = rigidbody.velocity.normalized; //Quaternion.Euler(this.rigidbody.velocity);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Target")
        {
            var temp = Tip.position - other.transform.position;
            temp.y = 0;
            var distance = temp.magnitude;
            if (distance <= 10)
            {
                var score = Mathf.Lerp(10, 0, distance / 10f);
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().Score += Mathf.CeilToInt(score);
                //Debug.Log("Distance " + distance + "Score " + score + " Ceiled " + Mathf.CeilToInt(score));
            }

            this.constantForce.enabled = false;
            this.rigidbody.velocity = Vector3.zero;
            this.rigidbody.useGravity = false;
            this.enabled = false;
        }

        if (other.tag == "Wall")
        {
            this.constantForce.enabled = false;
            this.rigidbody.velocity = Vector3.zero;
            this.rigidbody.useGravity = false;
            this.enabled = false;
        }

    }

}
