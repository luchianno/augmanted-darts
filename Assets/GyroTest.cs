﻿using UnityEngine;
using System.Collections;

public class GyroTest : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public float movementScale = 0.05f;

    void Update()
    {
        Vector3 pos = transform.position;
        pos.y = Vector3.Dot(Input.gyro.gravity, Vector3.up) * movementScale;
        transform.position = pos;
    }
}
